document.getElementById('task-form').addEventListener('submit', function(event) {
    event.preventDefault();

    const taskInput = document.getElementById('task-input');
    const taskList = document.getElementById('task-list');

    const newTask = document.createElement('li');
    newTask.innerHTML = `
        <input type="checkbox">
        <span class="mx-3">${taskInput.value}</span>
        <button class="delete-button text-red-500"><i class="fas fa-trash-alt"></i></button>
    `;

    taskList.appendChild(newTask);

    taskInput.value = '';

    const deleteButtons = document.querySelectorAll('.delete-button');
    deleteButtons.forEach(button => {
        button.addEventListener('click', function(event) {
            event.target.parentNode.parentNode.remove();
        });
    });
});
